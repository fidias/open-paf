package br.com.fidias.modelo.sped.blocoE;

import br.com.fidias.modelo.sped.Bean;
import java.util.Date;

/**
 *
 * @author atila
 */
public class DadosE100 extends Bean {
   
   private Date dt_ini, dt_fin;
   
   public DadosE100() {
      super("E100");
   }

   public Date getDt_ini() {
      return dt_ini;
   }

   public void setDt_ini(Date dt_ini) {
      this.dt_ini = dt_ini;
   }

   public Date getDt_fin() {
      return dt_fin;
   }

   public void setDt_fin(Date dt_fin) {
      this.dt_fin = dt_fin;
   }
}
