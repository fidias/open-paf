package br.com.fidias.modelo.sped.blocoE;

import br.com.fidias.modelo.sped.Bean;

/**
 *
 * @author atila
 */
public class DadosE110 extends Bean{
   
   private double vl_tot_debitos,
           vl_aj_debitos,
           vl_tot_aj_debitos,
           vl_estornos_cred,
           vl_tot_creditos,
           vl_aj_creditos,
           vl_tot_aj_creditos,
           vl_estornos_deb,
           vl_sld_credor_ant,
           vl_sld_apurado,
           vl_tot_ded,
           vl_icms_recolher,
           vl_sld_credor_transportar,
           deb_esp;
   
   public DadosE110() {
      super("E110");
   }

   public double getVl_tot_debitos() {
      return vl_tot_debitos;
   }

   public void setVl_tot_debitos(double vl_tot_debitos) {
      this.vl_tot_debitos = vl_tot_debitos;
   }

   public double getVl_aj_debitos() {
      return vl_aj_debitos;
   }

   public void setVl_aj_debitos(double vl_aj_debitos) {
      this.vl_aj_debitos = vl_aj_debitos;
   }

   public double getVl_tot_aj_debitos() {
      return vl_tot_aj_debitos;
   }

   public void setVl_tot_aj_debitos(double vl_tot_aj_debitos) {
      this.vl_tot_aj_debitos = vl_tot_aj_debitos;
   }

   public double getVl_estornos_cred() {
      return vl_estornos_cred;
   }

   public void setVl_estornos_cred(double vl_estornos_cred) {
      this.vl_estornos_cred = vl_estornos_cred;
   }

   public double getVl_tot_creditos() {
      return vl_tot_creditos;
   }

   public void setVl_tot_creditos(double vl_tot_creditos) {
      this.vl_tot_creditos = vl_tot_creditos;
   }

   public double getVl_aj_creditos() {
      return vl_aj_creditos;
   }

   public void setVl_aj_creditos(double vl_aj_creditos) {
      this.vl_aj_creditos = vl_aj_creditos;
   }

   public double getVl_tot_aj_creditos() {
      return vl_tot_aj_creditos;
   }

   public void setVl_tot_aj_creditos(double vl_tot_aj_creditos) {
      this.vl_tot_aj_creditos = vl_tot_aj_creditos;
   }

   public double getVl_estornos_deb() {
      return vl_estornos_deb;
   }

   public void setVl_estornos_deb(double vl_estornos_deb) {
      this.vl_estornos_deb = vl_estornos_deb;
   }

   public double getVl_sld_credor_ant() {
      return vl_sld_credor_ant;
   }

   public void setVl_sld_credor_ant(double vl_sld_credor_ant) {
      this.vl_sld_credor_ant = vl_sld_credor_ant;
   }

   public double getVl_sld_apurado() {
      return vl_sld_apurado;
   }

   public void setVl_sld_apurado(double vl_sld_apurado) {
      this.vl_sld_apurado = vl_sld_apurado;
   }

   public double getVl_tot_ded() {
      return vl_tot_ded;
   }

   public void setVl_tot_ded(double vl_tot_ded) {
      this.vl_tot_ded = vl_tot_ded;
   }

   public double getVl_icms_recolher() {
      return vl_icms_recolher;
   }

   public void setVl_icms_recolher(double vl_icms_recolher) {
      this.vl_icms_recolher = vl_icms_recolher;
   }

   public double getVl_sld_credor_transportar() {
      return vl_sld_credor_transportar;
   }

   public void setVl_sld_credor_transportar(double vl_sld_credor_transportar) {
      this.vl_sld_credor_transportar = vl_sld_credor_transportar;
   }

   public double getDeb_esp() {
      return deb_esp;
   }

   public void setDeb_esp(double deb_esp) {
      this.deb_esp = deb_esp;
   }
}
