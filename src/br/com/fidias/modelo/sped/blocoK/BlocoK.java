package br.com.fidias.modelo.sped.blocoK;

import br.com.fidias.modelo.sped.Bloco;
import java.io.FileWriter;

/**
 *
 * @author atila
 */
public class BlocoK implements Bloco {
   
   private DadosK001 dK001;
   private DadosK990 dK990;

   @Override
   public void gerar(FileWriter fw) throws Exception {
      if (dK001 != null) {
         dK001.gerar(fw);
      }
      
      if (dK990 != null) {
         dK990.gerar(fw);
      }
   }

   public DadosK001 getdK001() {
      return dK001;
   }

   public void setdK001(DadosK001 dK001) {
      this.dK001 = dK001;
   }

   public DadosK990 getdK990() {
      return dK990;
   }

   public void setdK990(DadosK990 dK990) {
      this.dK990 = dK990;
   }
}
