package br.com.fidias.modelo.sped.blocoK;

import br.com.fidias.modelo.sped.Bean;

/**
 *
 * @author atila
 */
public class DadosK990 extends Bean {
   
   private int qtd_lin;
   
   public DadosK990() {
      super("K990");
   }

   public int getQtd_lin() {
      return qtd_lin;
   }

   public void setQtd_lin(int qtd_lin) {
      this.qtd_lin = qtd_lin;
   }
}
