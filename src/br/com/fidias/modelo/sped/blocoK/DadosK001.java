package br.com.fidias.modelo.sped.blocoK;

import br.com.fidias.modelo.sped.Bean;

/**
 *
 * @author atila
 */
public class DadosK001 extends Bean {
   
   private int ind_mov;
   
   public DadosK001() {
      super("K001");
   }

   public int getInd_mov() {
      return ind_mov;
   }

   public void setInd_mov(int ind_mov) {
      this.ind_mov = ind_mov;
   }
}
