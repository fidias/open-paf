package br.com.fidias.modelo.sped.bloco0;

import br.com.fidias.modelo.sped.Bean;

public class Dados0001 extends Bean {

   private int ind_mov;

   public Dados0001() {
      super("0001");
   }

   public int getInd_mov() {
      return ind_mov;
   }

   public void setInd_mov(int ind_mov) {
      this.ind_mov = ind_mov;
   }
}
