package br.com.fidias.modelo.anexo.iii;

import br.com.fidias.modelo.anexo.Cabecalho;

/**
 * Classe que representa o modelo N1 do anexo III.
 *
 * @author Fidias
 */
public class N1 extends Cabecalho {

   public N1() {
      this.padrao = "N1";
   }
}
